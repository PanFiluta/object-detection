import cv2
import numpy as np
import tensorflow as tf
from flask import Flask, Response, render_template
import threading
import sys

# Check if the "--stream" argument was provided
stream = "--stream" in sys.argv
# Initialize the Flask application
app = Flask(__name__)

# Define the classifier for object detection
net = cv2.dnn.readNet("classifiers/yolov3.weights", "classifiers/yolov3.cfg")
face_cascade = cv2.CascadeClassifier('classifiers/haarcascade_frontalface_default.xml')

# Initialize the video capture from the camera
#cap = cv2.VideoCapture("VID_20230506_122103.mp4")
cap = cv2.VideoCapture("rtsp://admin:1234@192.168.2.59/Streaming/channels/001/?transportmode=unicast")
# Load the class labels for the COCO dataset
classes = []
with open("classifiers/coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]

# read haarcascade for number plate detection
cascade = cv2.CascadeClassifier('classifiers/haarcascade_russian_plate_number.xml')
# Define the generator function for video frames
def generate_frames():
    while True:
        success, frame = cap.read()
        if not success:
            break
        else:
            # Process every other frame
            if cap.get(cv2.CAP_PROP_POS_FRAMES) % 15 == 0:
                # Get the height and width of the frame
                height, width, _ = frame.shape

                # Convert the frame to a format suitable for object detection
                blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (288, 288), swapRB=True, crop=False)

                # Set the input layer of the neural network
                net.setInput(blob)

                # Get the output of the neural network
                output_layers_names = net.getUnconnectedOutLayersNames()
                layerOutputs = net.forward(output_layers_names)

                # Initialize the list of detected objects
                boxes = []
                confidences = []
                classIDs = []

                # Process the outputs of the neural network and get information about detected objects
                for output in layerOutputs:
                    for detection in output:
                        scores = detection[5:]
                        classID = np.argmax(scores)
                        confidence = scores[classID]
                        box = detection[0:4] * np.array([width, height, width, height])
                        (centerX, centerY, w, h) = box.astype("int")
                        x = int(centerX - (w / 2))
                        y = int(centerY - (h / 2))
                        boxes.append([x, y, int(w), int(h)])
                        confidences.append(float(confidence))
                        classIDs.append(classID)

                # Use non-maximum suppression to remove overlapping detections
                indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)

                # Draw bounding boxes and labels for detected objects in the frame
                for i in range(len(boxes)):
                    if i in indexes:
                        x, y, w, h = boxes[i]
                        label = str(classes[classIDs[i]])
                        #print(label)

                        # convert input image to grayscale
                        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                        # If the detected object is a person, crop and save the image
                        if label == "person":
                            # Draw the object bounding box and label in red
                            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)
                            cv2.putText(frame, label, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 255), 2)

                            # Crop the person's face and run face detection on it
                            face_img = frame[y:y+h, x:x+w]
                            gray_face_img = cv2.cvtColor(face_img, cv2.COLOR_BGR2GRAY)
                            faces = face_cascade.detectMultiScale(gray_face_img, scaleFactor=1.3, minNeighbors=5)
                            for (x_face, y_face, w_face, h_face) in faces:
                                cv2.rectangle(face_img, (x_face, y_face), (x_face+w_face, y_face+h_face), (0, 255, 0), 2)

                            # Save the image with the person's face
                            cv2.imwrite('person.jpg', face_img)


                        # If the detected object is a car, try to detect a license plate
                        if label == "car" or "truck":
                            # Loop over all license plates and draw bounding boxes around them
                            plates = cascade.detectMultiScale(gray, 1.2, 5)
                            for (x_plate, y_plate, w_plate, h_plate) in plates:
                                cv2.rectangle(frame, (x_plate, y_plate), (x_plate+w_plate, y_plate+h_plate), (0, 0, 255), 2)
                                # crop and save the license plate
                                gray_plates = gray[y_plate:y_plate+h_plate, x_plate:x_plate+w_plate]
                                cv2.imwrite('Numberplate.jpg', gray_plates)

                        # Draw the object bounding box and label in green
                        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        cv2.putText(frame, label, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), 2)



                # Convert the frame to JPEG format
                frame = cv2.imencode('.jpg', frame)[1].tobytes()

                yield (b'--myboundary\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n'+ frame + b'\r\n')

# Define the route for the video stream
if stream:
    @app.route('/')
    def index():
        return render_template('index.html')

    # Define the route for the video feed
    @app.route('/video_feed')
    def video_feed():
        return Response(generate_frames(), mimetype='multipart/x-mixed-replace; boundary=myboundary')
else:
    # Run the generator function without serving the video stream
    for frame in generate_frames():
        pass
# Define a list to store connected clients
clients = []

# Define the function to handle video stream for each client
def handle_client(client):
    for frame in generate_frames():
        try:
            client.send(frame)
        except Exception as e:
            print("Error sending frame to client:", str(e))
            clients.remove(client)
            break

# Define the route for multiple clients to access the video stream
@app.route('/multi_video_feed')
def multi_video_feed():
    return render_template('multi_index.html')

# Define the route for clients to connect to the video stream
@app.route('/stream')
def stream():
    return Response(stream_video(), mimetype='multipart/x-mixed-replace; boundary=myboundary')

# Define the function to handle multiple clients
def stream_video():
    client = request.environ.get('wsgi.websocket')
    if client:
        clients.append(client)
        while True:
            try:
                # Start a new thread to handle the video stream for this client
                t = threading.Thread(target=handle_client, args=(client,))
                t.daemon = True
                t.start()
                t.join()
            except Exception as e:
                print("Error handling client:", str(e))
                break

# Start the Flask application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=False)

