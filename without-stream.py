import cv2
import numpy as np
from datetime import datetime
import time
# Define the classifier for object detection
net = cv2.dnn.readNet("classifiers/yolov3.weights", "classifiers/yolov3.cfg")
face_cascade = cv2.CascadeClassifier('classifiers/haarcascade_frontalface_default.xml')

input_video = "rtsp://admin:1234@192.168.2.59/Streaming/channels/001/?transportmode=unicast"
#input_video = "VID_20230506_122103.mp4"
# Load the class labels for the COCO dataset
classes = []
with open("classifiers/coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]

# read haarcascade for number plate detection
cascade = cv2.CascadeClassifier('classifiers/haarcascade_russian_plate_number.xml')

# Define the generator function for video frames
def generate_frames():
    cap = cv2.VideoCapture(input_video)
    while True:
        now = datetime.now()
        timestamp = now.strftime("%Y-%m-%d_%H-%M-%S-%f")

        success, frame = cap.read()
        if not success:
            # Sleep for a short time to avoid flooding the output with error messages
            time.sleep(0.1)
            # Attempt to re-open the video input
            cap.release()
            cap = cv2.VideoCapture(input_video)
            success, frame = cap.read()
            if not success:
                continue
        else:
            # Process every other frame
            if cap.get(cv2.CAP_PROP_POS_FRAMES) % 15 == 0:
                # Get the height and width of the frame
                height, width, _ = frame.shape

                # Convert the frame to a format suitable for object detection
                blob = cv2.dnn.blobFromImage(frame, 1 / 255.0, (288, 288), swapRB=True, crop=False)

                # Set the input layer of the neural network
                net.setInput(blob)

                # Get the output of the neural network
                output_layers_names = net.getUnconnectedOutLayersNames()
                layerOutputs = net.forward(output_layers_names)

                # Initialize the list of detected objects
                boxes = []
                confidences = []
                classIDs = []

                # Process the outputs of the neural network and get information about detected objects
                for output in layerOutputs:
                    for detection in output:
                        scores = detection[5:]
                        classID = np.argmax(scores)
                        confidence = scores[classID]
                        box = detection[0:4] * np.array([width, height, width, height])
                        (centerX, centerY, w, h) = box.astype("int")
                        x = int(centerX - (w / 2))
                        y = int(centerY - (h / 2))
                        boxes.append([x, y, int(w), int(h)])
                        confidences.append(float(confidence))
                        classIDs.append(classID)

                # Use non-maximum suppression to remove overlapping detections
                indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)

                # Draw bounding boxes and labels for detected objects in the frame
                for i in range(len(boxes)):
                    if i in indexes:
                        x, y, w, h = boxes[i]
                        label = str(classes[classIDs[i]])
                        #print(label)

                        # convert input image to grayscale
                        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

                        # If the detected object is a person, crop and save the image
                        if label == "person":
                            # Draw the object bounding box and label in red
                            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)
                            cv2.putText(frame, label, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 255), 2)

                            # Crop the person
                            # Crop the person's face and run face detection on it
                            face_img = frame[y:y+h, x:x+w]
                            gray_face_img = cv2.cvtColor(face_img, cv2.COLOR_BGR2GRAY)
                            faces = face_cascade.detectMultiScale(gray_face_img, scaleFactor=1.3, minNeighbors=5)
                            for (x_face, y_face, w_face, h_face) in faces:
                                cv2.rectangle(face_img, (x_face, y_face), (x_face+w_face, y_face+h_face), (0, 255, 0), 2)

                            # Save the image with the person's face
                            cv2.imwrite(f'person_{timestamp}.jpg', face_img)
                            cv2.imwrite(f'person.jpg', face_img)


                        # If the detected object is a car, try to detect a license plate
                        if label == "car":
                            # Loop over all license plates and draw bounding boxes around them
                            plates = cascade.detectMultiScale(gray, 1.2, 5)
                            for (x_plate, y_plate, w_plate, h_plate) in plates:
                                cv2.rectangle(frame, (x_plate, y_plate), (x_plate+w_plate, y_plate+h_plate), (0, 0, 255), 2)
                                # crop and save the license plate
                                gray_plates = gray[y_plate:y_plate+h_plate, x_plate:x_plate+w_plate]
                                cv2.imwrite(f'Numberplate_{timestamp}.jpg', gray_plates)
                                cv2.imwrite('Numberplate.jpg', gray_plates)

                        # Draw the object bounding box and label in green
                        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
                        cv2.putText(frame, label, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), 2)

                # Show the processed frame
                cv2.imwrite('live.jpg', frame)


generate_frames()

